class UsuariosController < ApplicationController

  def show
	@usuario = Usuario.find(params[:id])
  end

  def nueva
  	@usuario = Usuario.new
  end

  def create
  	@usuario = Usuario.new(usuario_params)
  	if @usuario.save
  		flash[:success] = "Bienvenido, " + @usuario.nombre + "."" Comienza a enviar!"
  		redirect_to @usuario
  	else
  		render 'nueva'
  	end
  end

  private

  	def usuario_params
  		params.require(:usuario).permit(:nombre, :email, :password, :password_confirmation)
  	end

end
